import pandas as pd
import numpy as np
import re
import nltk
from sklearn import metrics
from sklearn.preprocessing import LabelEncoder
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer,TfidfTransformer
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import MultinomialNB 

#nltk.download('stopwords')
from nltk.corpus import stopwords
stop_words = stopwords.words('english')
wpt = nltk.WordPunctTokenizer()

def loadData():
	data = pd.read_csv("bbc-text.csv")
	
	text = data['text'].values
	for doc in text:
		doc = normalize(doc)
	category = data['category'].values
	encoder = LabelEncoder()
	encoder.fit(category)
	category = encoder.transform(category)

	X_train, X_test, y_train, y_test = train_test_split(text, category, test_size = 0.3, random_state = 42)
	return X_train, y_train, X_test, y_test

def normalize(document):
    # lower case and remove special characters\whitespaces
    document = re.sub(r'[^a-zA-Z\s]', '', document, re.I|re.A)
    document = document.lower()
    document = document.strip()

    # tokenize document
    tokens = wpt.tokenize(document)
    # filter stopwords out of document
    filtered_tokens = [token for token in tokens if token not in stop_words]
    # re-create document from filtered tokens
    document = ' '.join(filtered_tokens)

    return document

if __name__ == "__main__":
	X_train, y_train, X_test, y_test = loadData()

	steps = []
	steps.append(('CountVectorizer', CountVectorizer(ngram_range=(1,5),stop_words=stop_words,max_df=0.5, min_df=5)))
	steps.append(('tfidf', TfidfTransformer(use_idf=False, sublinear_tf = True,norm='l2',smooth_idf=True)))
	steps.append(('classifier', MultinomialNB()))
	clf = Pipeline(steps)
	clf.fit(X_train, y_train)

	y_pred = clf.predict(X_test)
	report1 = metrics.classification_report(y_test, y_pred, labels=[0,1,2,3,4], digits=3)
	print('TRAIN 70/30 \n\n',report1)
	print("Accuracy score: ", metrics.accuracy_score(y_test, y_pred))



