import pandas as pd
import numpy as np

from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics

def main():
	credit = pd.read_csv('credit_train.csv')
	#Drop id values
	credit.drop(labels=['Loan ID', 'Customer ID'], axis=1, inplace=True)

	## Missing Values Processing
	# Drop the columns with > 50% missing values
	credit.drop(columns = 'Months since last delinquent', axis=1, inplace=True)
	# Drop last 514 rows x 16 columns are NaN values
	credit.drop(credit.tail(514).index, inplace=True)
	# Drop missing values
	for i in credit['Maximum Open Credit'][credit['Maximum Open Credit'].isnull() == True].index:
		credit.drop(labels=i, inplace=True)
	for i in credit['Tax Liens'][credit['Tax Liens'].isnull() == True].index:
		credit.drop(labels=i, inplace=True)
	for i in credit['Bankruptcies'][credit['Bankruptcies'].isnull() == True].index:
		credit.drop(labels=i, inplace=True)
    # Fill missing values
	credit.fillna(credit.mean(), inplace=True)
	credit.fillna('10+ years', inplace=True)

	##Feature Exacting
	#One-hot vector encode
	#Category
	categorical_subset = credit[['Term', 'Years in current job', 'Home Ownership', 'Purpose']]
	categorical_subset = pd.get_dummies(categorical_subset)
	credit.drop(labels=['Term', 'Years in current job', 'Home Ownership', 'Purpose'], axis=1, inplace=True)
	credit = pd.concat([credit, categorical_subset], axis = 1)
	#Label
	le = preprocessing.LabelEncoder()
	credit['Loan Status'] = le.fit_transform(credit['Loan Status'])

	##Train test spliting
	label = credit['Loan Status']
	data = credit.drop(columns='Loan Status')
	X_train, X_test, y_train, y_test = train_test_split(data, label, test_size = 0.3, random_state = 42)

	##Training
	clf = DecisionTreeClassifier()
	clf.fit(X_train, y_train)

	##Testing
	y_pred = clf.predict(X_test)
	report1 = metrics.classification_report(y_test, y_pred, labels=[0,1], digits=3)
	print('TRAIN 70/30 \n\n',report1)
	print("Accuracy score: ", metrics.accuracy_score(y_test, y_pred))

main()